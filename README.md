# Elternvertreter

Eine App für Elternvertreter an Schulen zum verwalten der Aufgaben und Kontakte in der Klasse. 

* Plattform übergreifend
    * Windows
    * Android
    * iOS/macOS
    * Linux


## Funktionen
### Kontakte
* Kontakte Karten Kinder basiert.
    * Schulkontakte (Lehrer, Erzieher)
        * Typ
        * Name
        * E-Mail
        * Telefon
    * pro Kind: 
        * Name
        * Telefon (Optional)
        * E-Mail (Optional)
        * BuT von/bis (optional)
        * Notiz  (Optional)
        * Kontaktpersonen
            * Art (Mutter, Vater, andere)
            * Name 
            * Telefon (Optional)
            * E-Mail  (Optional)
            * Adresse (Optional)
            * Notiz (Optional
* Alle Kontaktdaten Felder mit Checkbox für Weitergabe an Dritte erlaubt
* Funktionen
    * wenn Weitergabe erlaubt dann Teilen Button anzeigen
    * Telefon: Nachricht schreiben, Anrufen
    * E-Mail: Nachricht schreiben
* Datenschutz sicheres editieren.

### Mail Versand
* Auswahl von Kontakten
    * Alle Kinder Kontaktpersonen
    * Auswahl Kinder Kontaktpersonen
    * Alle Kinder
    * Auswahl Kinder
* öffne Standard Mail Client
* Setzte Kontakte als BCC Feld
* Wenn möglich Anhang mit auswählbar machen
* Wenn möglich Betreff mit eingeben

### Dokumente
* Hochladen (wo speichern?, welche Formate erlaubt?)
* Anzeigen (PDF)
* Schreiben (speichern als PDF)
* Löschen
* Dokumente Teilen -> Auswahl Kontakte siehe Mail Versand
* Kategorien verwalten (Standard: Protokolle, Einladungen)

### Klassenkasse
* Verwalten der Klassenkasse mit Einnahmen und Ausgabe
* Eingabe aktueller Beitrag für Klasse x
* Einahmen
    * Auswahl Kind
    * Eingabe Betrag (Standardbetrag als Default anzeige)
* Ausgaben
    * Eingabe
        * Wofür
        * Wer hat das Geld entgegen genommen
        * Wann 
        * BuT fähig
        * Auswahl welche Kinder waren "nicht" dabei
* Übersicht
    * Ein/Ausgaben Summierungen
        * Gesamt
        * Pro Kind
        * Pro Ausgabe
### Einstellungen
* Allgemeines setzen der aktuellen Klasse und des Schuljahres
* Daten export 
    * Verschlüsseln
    * Teilen
* allgemines verschlüsseltes Datensharing via
    * Cloud Anbieter
    * ftp
    * http
    * ssh
    * ???

## Authors and acknowledgment


## License
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Elternvertreter</span> von <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">Michael Graß</span> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz</a>.

## Project status
* In der Planungsphase
    * Hilfe ist willkommen


## Project Development Setup
* install nodejs latest
* install androidsdk/android studio latest (java required)
* set env  ANDROID_SDK_ROOT to android sdk root
    * add emulator, platform-tools, tools/bin, build-tools/%version% folders to the PATH variable
* install ionic `npm install -g @ionic/cli`

* Run `ionic serve` within the app directory to see your app in the browser
* Run `ionic capacitor add` to add a native iOS or Android project using Capacitor
    * https://capacitorjs.com/docs/basics/workflow
* Generate your app icon and splash screens using cordova-res --skip-config --copy
* Explore the Ionic docs for components, tutorials, and more: https://ion.link/docs
